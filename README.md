# README #

### What is this repository for? ###

* Creates a basic Eureka Server running Spring-Boot
* 1.0.0

### How do I get set up? ###

* $ cd <working_directory>
* $ mvn spring-boot:run
* Launch browser: http://localhost:8761

### Example on Heroku ###

* For example Eureka dashboard visit: https://eureka-server-spring-boot.herokuapp.com/