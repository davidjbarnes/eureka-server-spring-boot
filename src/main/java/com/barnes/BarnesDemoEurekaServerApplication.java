package com.barnes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class BarnesDemoEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BarnesDemoEurekaServerApplication.class, args);
    }
}
